package petrov.autotest.page;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.List;

public class MapsYandexPage {

    @FindBy(css = ".catalog-small-item-view")
    private List<WebElement> mapsLinkList;


    public WebDriver driver;

    public MapsYandexPage(WebDriver driver){
        PageFactory.initElements(driver, this);
        this.driver = driver;
    }


    public int getMapsLinkListSize(){
        return mapsLinkList.size();
    }

}
