package petrov.autotest.page;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

public class CompareYandexMarketPage {

    @FindBy(css = ".n-compare-head__name")
    private List<WebElement> productsNameList;
    @FindBy(css = ".n-compare-toolbar__action-clear")
    private WebElement clearButton;

    public WebDriver driver;

    public CompareYandexMarketPage(WebDriver driver){
        PageFactory.initElements(driver, this);
        this.driver = driver;
    }


    public int getProductsCount(){
        WebElement explicitWait = (new WebDriverWait(driver, 10))
                .until(ExpectedConditions.elementToBeClickable(clearButton));
        return productsNameList.size();
    }

    public int getProductsCountClear(){
        Boolean explicitWait = (new WebDriverWait(driver, 10))
                .until(ExpectedConditions.invisibilityOf(productsNameList.get(0)));
        return productsNameList.size();
    }

    public void clearCompareList(){
        clearButton.click();
    }

}
