package petrov.autotest.page;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class LaptopYandexMarketPage {

    @FindBy(css = ".n-snippet-card2__title")
    private List<WebElement> productsNameList;
    @FindBy(css = ".link .price")
    private List<WebElement> productsPriceList;
    @FindBy(css = ".button_theme_normal")
    private WebElement listSizesSelectButton;
    @FindBy(css = ".select__list div:first-child")
    private WebElement oneSizeProductsSelectItem;
    @FindBy(css = ".select__list div:last-child")
    private WebElement twoSizeProductsSelectItem;
    @FindBy(css = ".n-filter-sorter_js_inited:nth-of-type(3)")
    private WebElement priceSortingButton;
    @FindBy(css = ".n-filter-sorter_sort_desc, .n-filter-sorter_sort_asc")
    private WebElement priceSortingEnabledButton;
    @FindBy(css = ".n-snippet-card2:nth-of-type(1) .image_name_compare")
    private WebElement addToCompareFirstButton;
    @FindBy(css = ".n-snippet-card2:nth-of-type(2) .image_name_compare")
    private WebElement addToCompareSecondButton;
    @FindBy(css = ".header2-menu__item_type_compare")
    private WebElement compareButton;


    public WebDriver driver;

    public LaptopYandexMarketPage(WebDriver driver){
        PageFactory.initElements(driver, this);
        this.driver = driver;
    }


    public void selectListProductSizeOne(){
        Actions actions = new Actions(driver);
        actions.moveToElement(listSizesSelectButton).build().perform();
        listSizesSelectButton.click();
        elementWaitVisibilityAndClick(oneSizeProductsSelectItem);
    }

    public void selectListProductSizeTwo(){
        Actions actions = new Actions(driver);
        actions.moveToElement(listSizesSelectButton).build().perform();
        listSizesSelectButton.click();
        elementWaitVisibilityAndClick(twoSizeProductsSelectItem);
    }

    public int getProductsListCount(){
    Boolean explicitWait = (new WebDriverWait(driver, 10))
            .until(ExpectedConditions.invisibilityOf(productsNameList.get(0)));
        return productsNameList.size();
    }

    public void priceSortingButtonClick(){
        priceSortingButton.click();
    }

    public Boolean isVisiblePriceSortingEnableButton(){
        return priceSortingEnabledButton.isDisplayed();
    }

    public Boolean valideListProductsPriceSorting(){
        Boolean isSortedB = true;
        Boolean isSortedL = true;
        List<Integer> intPriceList = getListProductsIntPrice();
        int intPrice = intPriceList.get(0);

        for (int tempPrice : intPriceList) {
            if (intPrice < tempPrice){
                isSortedB = false;
            }
        }
        for (int tempPrice : intPriceList) {
            if (intPrice > tempPrice){
                isSortedL = false;
            }
        }
        return isSortedB | isSortedL;
    }

    public void addToCompareProductFirstInList(){
        Actions actions = new Actions(driver);
        actions.moveToElement(productsNameList.get(0)).build().perform();
        elementWaitVisibilityAndClick(addToCompareFirstButton);

    }

    public void addToCompareProductSecondInList(){
        Actions actions = new Actions(driver);
        actions.moveToElement(productsNameList.get(1)).build().perform();
        elementWaitVisibilityAndClick(addToCompareSecondButton);

    }

    public void compareButtonClick(){
        compareButton.click();
    }


    private List<Integer> getListProductsIntPrice(){
        String strPrice;
        List<Integer> intPriceList = new ArrayList<Integer>();

        Boolean explicitWait = (new WebDriverWait(driver, 10))
                .until(ExpectedConditions.invisibilityOf(productsNameList.get(0)));
        Pattern pat=Pattern.compile("[0-9]");
        for (WebElement price : productsPriceList){
            strPrice = "";
            Matcher matcher=pat.matcher(price.getText());
            while (matcher.find()) {
                strPrice = strPrice + matcher.group();
            }
            intPriceList.add(Integer.parseInt(strPrice));
        }
        return intPriceList;
    }

    private void elementWaitVisibilityAndClick(WebElement webElement){
        WebElement explicitWait = (new WebDriverWait(driver, 10))
                .until(ExpectedConditions.visibilityOf(webElement));
        webElement.click();
    }
}
