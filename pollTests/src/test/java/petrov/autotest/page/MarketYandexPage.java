package petrov.autotest.page;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.List;

public class MarketYandexPage {

    @FindBy(css = "._3Sl8p9JEFz")
    private List<WebElement> marketLinkList;


    public WebDriver driver;

    public MarketYandexPage(WebDriver driver){
        PageFactory.initElements(driver, this);
        this.driver = driver;
    }


    public int getMarketLinkListSize(){
        return marketLinkList.size();
    }

}
