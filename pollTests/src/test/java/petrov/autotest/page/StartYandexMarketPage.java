package petrov.autotest.page;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class StartYandexMarketPage {

    @FindBy(css = ".topmenu__item[data-department = 'Компьютеры']")
    private WebElement computerTopMenuItem;
    @FindBy(css = "a[href = '/catalog/54544/list?hid=91013&track=menuleaf']")
    private WebElement laptopTopMenuSubItem;
    @FindBy(css = ".n-region-notification__ok")
    private WebElement regionYesButton;


    public WebDriver driver;

    public StartYandexMarketPage(WebDriver driver){
        PageFactory.initElements(driver, this);
        this.driver = driver;
    }


    public void selectLaptopTopMenuSubItem(){
        if (regionYesButton.isEnabled()){
            regionYesButton.click();
        }
        Actions actions = new Actions(driver);
        actions.moveToElement(computerTopMenuItem).build().perform();
        //actions.click(laptopTopMenuSubItem);
        /*Action mouseoverAndClick = actions.build();
        mouseoverAndClick.perform();*/
        WebElement explicitWait = (new WebDriverWait(driver, 5))
                .until(ExpectedConditions.elementToBeClickable(laptopTopMenuSubItem));
        laptopTopMenuSubItem.click();
    }

}
