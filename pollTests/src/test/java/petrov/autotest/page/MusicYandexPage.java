package petrov.autotest.page;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.List;

public class MusicYandexPage {

    @FindBy(css = ".d-track")
    private List<WebElement> musicLinkList;


    public WebDriver driver;

    public MusicYandexPage(WebDriver driver){
        PageFactory.initElements(driver, this);
        this.driver = driver;
    }


    public int getMusicLinkListSize(){
        return musicLinkList.size();
    }

}
