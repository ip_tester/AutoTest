package petrov.autotest.page;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.List;

public class VideoYandexPage {

    @FindBy(css = ".carousel__item")
    private List<WebElement> videoLinkList;


    public WebDriver driver;

    public VideoYandexPage(WebDriver driver){
        PageFactory.initElements(driver, this);
        this.driver = driver;
    }


    public int getVideoLinkListSize(){
        return videoLinkList.size();
    }

}
