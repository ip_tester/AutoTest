package petrov.autotest.page;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class HomeMenuYandexPage {

    @FindBy(css = ".home-tabs__search[data-id = 'video']")
    private WebElement videoButton;
    @FindBy(css = ".home-tabs__search[data-id = 'images']")
    private WebElement imagesButton;
    @FindBy(css = ".home-tabs__search[data-id = 'news']")
    private WebElement newsButton;
    @FindBy(css = ".home-tabs__search[data-id = 'maps']")
    private WebElement mapsButton;
    @FindBy(css = ".home-tabs__search[data-id = 'market']")
    private WebElement marketButton;
    @FindBy(css = ".home-tabs__search[data-id = 'translate']")
    private WebElement translateButton;
    @FindBy(css = ".home-tabs__search[data-id = 'music']")
    private WebElement musicButton;


    public WebDriver driver;

    public HomeMenuYandexPage(WebDriver driver){
        PageFactory.initElements(driver, this);
        this.driver = driver;
    }


    public void videoButtonClick(){
        videoButton.click();
    }
    public String getLinkVideoButton(){
        return videoButton.getAttribute("href")+"/";
    }
    public void imagesButtonClick(){
        imagesButton.click();
    }
    public String getLinkImagesButton(){
        return imagesButton.getAttribute("href");
    }
    public void newsButtonClick(){
        newsButton.click();
    }
    public String getLinkNewsButton(){
        return newsButton.getAttribute("href");
    }
    public void mapsButtonClick(){
        mapsButton.click();
    }
    public String getLinkMapsButton(){
        return mapsButton.getAttribute("href");
    }
    public void marketButtonClick(){
        marketButton.click();
    }
    public String getLinkMarketButton(){
        return marketButton.getAttribute("href");
    }
    public void translateButtonClick(){
        translateButton.click();
    }
    public String getLinkTranslateButton(){
        return translateButton.getAttribute("href");
    }
    public void musicButtonClick(){
        musicButton.click();
    }
    public String getLinkMusicButton(){
        return musicButton.getAttribute("href") + "home";
    }
}
