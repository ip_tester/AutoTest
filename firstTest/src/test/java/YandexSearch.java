import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.concurrent.TimeUnit;

public class YandexSearch {

    private static WebDriver driver;

    @BeforeTest
    public static void setup() {

        System.setProperty("webdriver.chrome.driver", "chromedriver.exe");

        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);

    }

    @Test
    public void SearchTestWord(){

        driver.get("https://ya.ru/");
        WebElement searchInput = driver.findElement(By.id("text"));
        searchInput.clear();
        searchInput.sendKeys("погода пенза");
        String firstLinkString = driver.findElement(By.cssSelector(".popup__content li:first-child b")).getText();
        Assert.assertTrue(firstLinkString.contains("погода"));
        Assert.assertTrue(firstLinkString.contains("пенза"));
    }

    @AfterTest
    public static void testQuit(){

        driver.quit();

    }

}
