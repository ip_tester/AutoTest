package petrov.autotest;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class SearchPage {

    @FindBy(id = "text")
    private WebElement searchInput;

    @FindBy(css = ".popup__content li:first-child b")
    private WebElement firstLinkString;

    public void init(final WebDriver driver){
        PageFactory.initElements(driver, this);
    }

    public void search(String text){
        searchInput.sendKeys(text);
    }

    public String getResult(){
        return firstLinkString.getText();
    }
}
